package com.cola.colaboot.module.design.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cola.colaboot.module.design.pojo.DesignImgGroup;

public interface DesignImgGroupService extends IService<DesignImgGroup> {
    IPage<DesignImgGroup> pageList(DesignImgGroup group, Integer pageNo, Integer pageSize);
}
