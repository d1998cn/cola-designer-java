package com.cola.colaboot.module.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cola.colaboot.module.system.pojo.SysUser;

public interface SysUserService extends IService<SysUser> {
    SysUser getByUsername(String userName);

    SysUser getByPhone(String userName);

    IPage<SysUser> pageList(SysUser sysUser, Integer pageNo, Integer pageSize);
}
