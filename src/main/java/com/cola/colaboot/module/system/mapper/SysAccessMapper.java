package com.cola.colaboot.module.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cola.colaboot.module.system.pojo.SysAccess;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface SysAccessMapper extends BaseMapper<SysAccess> {
    @Select("SELECT `sys_access`.* FROM `sys_access` inner join sys_role_access on sys_access.id=sys_role_access.access_id " +
            "where sys_role_access.role_id = (select role_id from sys_user where username = #{username})")
    List<SysAccess> getAccessByUserName(String username);

    List<SysAccess> listRoleAccessByParent(SysAccess access, Integer roleId);

    List<SysAccess> treeRoleAccess(@Param("roleId") Integer roleId,@Param("menuType") Integer menuType);

    @Select("SELECT `sys_access`.* FROM `sys_access` inner join sys_role_access on sys_access.id=sys_role_access.access_id " +
            "where parent_id = #{parentId} and sys_role_access.role_id = #{roleId}")
    List<SysAccess> getChildren(@Param("parentId") String parentId, @Param("roleId") Integer roleId);

    @Select("SELECT `sys_access`.id FROM `sys_access` inner join sys_role_access on sys_access.id=sys_role_access.access_id " +
            "where sys_role_access.role_id = #{roleId}")
    List<Integer> getAccessIdsByRole(Integer roleId);
}
