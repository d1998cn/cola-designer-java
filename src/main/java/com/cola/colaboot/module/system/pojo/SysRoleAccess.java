package com.cola.colaboot.module.system.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("sys_role_access")
public class SysRoleAccess {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private Integer roleId;
    private Integer accessId;

    public SysRoleAccess() {
    }

    public SysRoleAccess(Integer roleId, Integer accessId) {
        this.roleId = roleId;
        this.accessId = accessId;
    }
}