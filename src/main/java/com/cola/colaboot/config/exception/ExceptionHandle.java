package com.cola.colaboot.config.exception;

import com.cola.colaboot.config.dto.Res;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionHandle {
    private static final Logger log = LoggerFactory.getLogger(ExceptionHandle.class);

    @ExceptionHandler(value = Exception.class)
    public Res<?> javaExceptionHandler(Exception ex) {
        if (ex instanceof ColaException){
            log.error("捕获到业务异常:"+ex.getMessage());
            return Res.fail(ex.getMessage());
        }else if(ex instanceof LoginException){
            return Res.fail(301, ex.getMessage());
        }
        ex.printStackTrace();
        return Res.fail("服务器错误"+ex.getMessage());
    }
}
